# docker of Ubiquiti Unifi Controller [latest]


## acquisition of container image

For a quick start use the docker hub method, for access to the source, built it yourself.



### pull from docker hub


```	
	docker pull fcogomez/unifi-controller
```

(see instructions below for running it)




### building docker image

```
git clone https://github.com/rednut/docker-unifi-controller.git 
cd docker-unifi-controller
make
```


## run the container: launching the unifi controller daemon

- to launch a container using the image created earlier:

```
    docker run -d \
            -p 8080:8080 -p 8443:8443 -p 8880:8880 -p 37117:27117 \
            -v /srv/data/apps/docker/unifi/data:/usr/lib/unifi/data \
            --name unifi rednut/unifi-controller
```


### notes on the make / build of the container

The Makefile will provision the docker container image from the Dockerfile which will provision the image with upstream ubuntu:latest and include all the required dependencies to run the the unifi controller.

The unifi controller repository will provide the .debs. The package requires mongodb.

## run the container: launching the unifi controller daemon

### volumes for persistent data

You can mount a local volume path into the container at `/usr/lib/unifi/data` by supplying to docker the `-v` argument like `-v <local_path>:<container_path>` or `-v /srv/data/apps/docker/unifi/data:/usr/lib/unifi/data`

### ports

To connunicate with the unifi controller you can map various ports, eg:

- 8080: non tls web ui
- 8443: tls web ui
- 8880: guest login ui
- 27117: mongo 

### command to run the unifi controller daemon

To launch a container using the image created earlier:

``` 
	docker run -d \
			-p 8080:8080 -p 8443:8443 -p 8880:8880 -p 37117:27117 \
			-v /srv/data/apps/docker/unifi/data:/usr/lib/unifi/data \
			--name unifi fcogomez/unifi-controller
```


## management of container 

see Makefile...

- check the container is runing:
	`docker ps`

- check logs from container:
	`docker logs unifi`

- show process:
	`docker top unifi`

- kill the container ie stop process and stop container:
	`docker kill unifi`

- remove named conatiner (so you can re-run it):
	`docker rm unifi`

so its usually better, after running the container to just stop/start it instead:

- pause / unpause aka suspend running cotainer:

	`docker pause/unpause unifi`

	`docker restart unifi`

	`docker stop/start unifi`

	`docker kill unifi`

# Changelog

- 20161111 Copied working unifi-controller docker from rednut.


